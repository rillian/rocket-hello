FROM docker.io/library/rust as builder

WORKDIR /src
COPY . .
RUN cargo install --path .

FROM docker.io/library/debian as runner
COPY --from=builder /usr/local/cargo/bin/rocket-hello /usr/local/bin/

ENV ROCKET_ADDRESS=0.0.0.0
ENTRYPOINT /usr/local/bin/rocket-hello
